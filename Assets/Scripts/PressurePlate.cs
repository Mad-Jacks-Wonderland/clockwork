﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PressurePlate : MonoBehaviour
{
    // script ofr pressure plates
    public bool IsPressed = false;
  

    private void OnTriggerEnter(Collider col) // if something is intersecting with them, they activate
    {
        IsPressed = true;
        
    }

    private void OnTriggerExit(Collider col) // deactivated when the collider has nothing in it
    {
        IsPressed = false;
    }

   


}
