﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;

public class AudioManager : MonoBehaviour
{
    
    public Sound[] sounds; // this will create a lost of sounds, which will have all the properties from the class

    // Start is called before the first frame update
    void Awake()
    {
        foreach (Sound s in sounds) //this loop loads all of the sound assets and their "settings"
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }

        FindObjectOfType<AudioManager>().Play("BgMusic"); // This will start playing background ambience music when the game starts
    }

    // Update is called once per frame
    public void Play(string name) // This function will e used to play sounds from different scripts
    {
        Sound s = Array.Find(sounds, sound => sound.name == name); // it will look if the name given fits with any of the objects' name
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found"); // it sill display the message is such has not been found
            return;
        }
        s.source.Play(); // if everything works well, it will play the sound, with all of its properties applied in it
    }
}
