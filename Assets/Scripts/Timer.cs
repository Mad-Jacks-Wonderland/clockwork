﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    // simple timer made for the sake of the game
    float currentTime = 0f;
    float startingTime = 300f;

    [SerializeField]
    Text countdownText; // text set up to display

    private void Start()
    {
        currentTime = startingTime; // setting up updating
    }

    private void Update()
    {
        currentTime -= 1 * Time.deltaTime; // subtract 1 each update which takes around 1 second to do
        countdownText.text = currentTime.ToString("0"); // rounding up the numbers to integers

        if(currentTime <= 0) // the timer does not count in negative numbers thanks to this function
        {
            currentTime = 0;
            FindObjectOfType<GameManagerScript>().EndGame(); // if clock hits 0, you lost
        }
    }

    public float GetTime() // getter if I would need one
    {
        return currentTime;
    }
}
