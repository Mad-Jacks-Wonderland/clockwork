﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelExit : MonoBehaviour
{
    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player") // a piece fo code which is attached to arb at the end of the level, which is the goal
        {
            
            FindObjectOfType<GameManagerScript>().GameWinScreen(); // is it senses player within its boundaries, it will turn on the Win screen
        }

    }
}
