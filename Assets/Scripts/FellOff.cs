﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FellOff : MonoBehaviour
{
    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player") // the code will check for the tag of the objec it collides with
        {

            FindObjectOfType<GameManagerScript>().EndGame(); // this script will load a game over screen if player fell off the map
        }

    }
}
