﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door2Script : DoorController
{
    
    private ButtonSctipt b1; // create button variables
    private ButtonSctipt b2;
    private ButtonSctipt b3;
    private ButtonSctipt b4;
    private ButtonSctipt b5;

    // Update is called once per frame
    void Update()
    {
        b1 = input1.GetComponent<ButtonSctipt>(); // assign the buttons
        b2 = input2.GetComponent<ButtonSctipt>();
        b3 = input3.GetComponent<ButtonSctipt>();
        b4 = input4.GetComponent<ButtonSctipt>();
        b5 = input5.GetComponent<ButtonSctipt>();

        if (b1.IsPressed == true && //open play sound and change door's position if all 5 are pressed;
            b2.IsPressed == true && 
            b3.IsPressed == true && 
            b4.IsPressed == true && 
            b5.IsPressed == true && 
            Opened == false)
        {
            Door.transform.position = new Vector3(-225, 621, 40);
            Opened = true;
            FindObjectOfType<AudioManager>().Play("PickUpSound");
        }
       
    }
}
