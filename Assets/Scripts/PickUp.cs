﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public Transform theDest;

    private void OnMouseDown()
    {
        
        GetComponent<BoxCollider>().enabled = false; // disbale collisions (easier to move around)
        GetComponent<Rigidbody>().useGravity = false; // disable gravity so it stays in the air in front of the player, imitating it being carried
        this.transform.position = theDest.position; // it sets its position ot a place in front of the player
        this.transform.parent = GameObject.Find("PickUpArea").transform;
        FindObjectOfType<AudioManager>().Play("PickUpSound"); // playes pick up sound

    }

    private void OnMouseUp()
    {
        this.transform.parent = null;
        GetComponent<Rigidbody>().useGravity = true; // return gravity when released
        GetComponent<BoxCollider>().enabled = true; // return collisions when released
    }
}
