﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour
{
    bool GameOver = false;
    bool GameWon = false;
    public GameObject gameWonUI; // the gameObject for the panel that appears when the player won the game
    public GameObject gameOverUI; // the gameObject for the panel that appears when the player lost the game


    public void EndGame() // this function can be called from any script of the sake of easy access
    {
        if(GameOver == false)
        {
            GameOver = true;
            gameOverUI.SetActive(true); // The UI game over screen will turn itself on at this point
            Invoke("ExitGame", 5f); // invoke will make the script move to ExitGame script
        }


    }

    public void GameWinScreen() // in case the player wins, this function is called
    {
        if (GameWon == false)
        {
            GameWon = true;
            gameWonUI.SetActive(true); // displays vague victory message and gives player a thank you message
            Invoke("ExitGame", 5f); // calls ExitGame function after 5 seconds
        }
    }

    public void ExitGame() // this function is there just to call it using invoke, which will simply close the game.
    {
        Debug.Log("Closing game");
        Application.Quit();
    }
    
    
    

}
