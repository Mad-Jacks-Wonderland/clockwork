﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door3Script : DoorController
{
    private Animation anim;
    private ButtonSctipt b1;
    private ButtonSctipt b2;
    private ButtonSctipt b3;
    private PressurePlate p1;
    private PressurePlate p2;
    private PressurePlate p3;

    // Update is called once per frame
    void Update()
    {
        // getting a reference to the input pressure plates and buttons
        b1 = input1.GetComponent<ButtonSctipt>();
        b2 = input2.GetComponent<ButtonSctipt>();
        b3 = input3.GetComponent<ButtonSctipt>();
        p1 = input4.GetComponent<PressurePlate>();
        p2 = input5.GetComponent<PressurePlate>();
        p3 = input6.GetComponent<PressurePlate>();
        anim = gameObject.GetComponent<Animation>();

        if (b1.IsPressed == true && // if all have been pressed but the door is not open yet, open it, play sound and change its position
            b2.IsPressed == true &&
            b3.IsPressed == true &&
            p1.IsPressed == true &&
            p2.IsPressed == true &&
            p3.IsPressed == true &&
            Opened == false)
        {
            Door.transform.position += new Vector3(0, -6, 0); // change the platform's position
            Opened = true; // set to open
            FindObjectOfType<AudioManager>().Play("DoorOpen"); // play sound
        }

    }

}
