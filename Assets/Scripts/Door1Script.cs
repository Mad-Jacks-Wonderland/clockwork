﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door1Script : DoorController
{

    private PressurePlate p1; // create variables for all 3 pressure plates
    private PressurePlate p2;
    private PressurePlate p3;


    void Update()
    {
        p1 = input1.GetComponent<PressurePlate>(); // assign pressure plate objects to variables
        p2 = input2.GetComponent<PressurePlate>();
        p3 = input3.GetComponent<PressurePlate>();

        if (p1.IsPressed == true && p2.IsPressed == true && p3.IsPressed == true && Opened == false) // is all 3 are pressed, and door is still closed, open change position and play sound
        {
            Door.transform.position = new Vector3(136, 656, -5);
            Opened = true;
            FindObjectOfType<AudioManager>().Play("DoorOpen");
        }
        else if (p1.IsPressed == false && Opened == true ||
                p2.IsPressed == false && Opened == true ||
                p3.IsPressed == false && Opened == true)
        {
            Door.transform.position += new Vector3(-146, 656, -5);
        }
    }
}
