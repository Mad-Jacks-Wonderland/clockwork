﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSctipt : MonoBehaviour
{
    // this is a code for the button objects

    public bool IsPressed = false;


    private void OnTriggerEnter(Collider col)
    {
        if(IsPressed == false && col.tag == "Player") // if it interacts with the player within its collider, it is pressed and plays sound
        {
            IsPressed = true;
            FindObjectOfType<AudioManager>().Play("ButtonActivation");
        }
        
    }


}
