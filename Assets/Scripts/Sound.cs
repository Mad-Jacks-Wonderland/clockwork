﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound
{
    // sound class
    public string name; // setting up the data and the settings of the sound

    public bool loop;

    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume; // definging the setting and its minimum and maximum values

    [Range(.1f, 3f)]
    public float pitch;

    [HideInInspector]
    public AudioSource source; // setting up audio source
}
