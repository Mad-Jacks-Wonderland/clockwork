﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // if play button is pressed, the level will load
    }

    public void QuitGame()
    {
        Debug.Log("Closing game..."); // if quit button is pressed, quit the application
        Application.Quit();
    }
}
